# Script to re-encode video files

This is a command line python script used to re-encode videos from a given directory to a specific video format, using [ffmpeg](https://ffmpeg.org/).  

This script will execute the following steps:  

1. Navigate the directory tree, locating all video files, based on the file extension.  
2. For each located video file, check if it has an already converted or if there is an associated skipped file
3. Use `ffmpeg` to extract the codec and media information for each file.
4. Check if the codec matches the required one, any video encoded with a different codec will be re-encoded

## Requirements

1. Python 3
2. ffmpeg, preferably with support to [encode H265 videos](https://trac.ffmpeg.org/wiki/Encode/H.265)

## Configurations

The script params are configured on the `video_auto_encode.properties` file.  
The default configuration is to encode the files to **H265 HEVC** format, which is a bit slower then H264 (world's default coded) but can achieve greater compression ratios (up to 75%).  
The files encodec with this configuration will have a **_H265.mp4** suffix.  
From the above example, the values `${INPUT}` and `${OUTPUT}` will be replaced by the input and output file by the script.

Default video_auto_encode.properties file:  

```properties
video_extensions=wmv|avi|mpg|3gp|mov|m4v|mts|mp4|webm|mkv|asf
path_to_ffmpeg=ffmpeg
ignored_file_suffix=.skip
remove_text_from_file_name=converted|compressed|-estabilizado|rotated|jpeg|MP4|H264_NV|H264|H265|VP9|.|__
replace_text_from_file_name=__|.| |-
replace_text_from_file_name_to=_

# For H265 videos
ffmpeg_encode_command=-i ${INPUT} -c:v libx265 -c:a aac -strict 2 -y -hide_banner -f mp4 ${OUTPUT}
encoded_file_suffix=_H265.mp4
encoded_file_codec_name=hevc

#For H264 using Nvidia hardware (see: https://trac.ffmpeg.org/wiki/HWAccelIntro)
#ffmpeg_encode_command=-hwaccel vdpau -i ${INPUT} -c:v h264_nvenc -c:a aac -strict 2 -y -hide_banner -f mp4 ${OUTPUT}
#encoded_file_suffix=_H264.mp4
#encoded_file_codec_name=h264

```

### Configurations properties

+ video_extensions

> Holds the list of extensions, delimited by a pipe `|`, that the script will consider as a video file.
>
> To add support to more video types, just add the file extension to this property.  
>
> Check the list of supported codes using `ffmpeg -codecs` or visit the [ffmpeg codec pages](https://trac.ffmpeg.org/)

+ path_to_ffmpeg

> Path to the `ffmpeg` application, this can be the full path or just the application name if `ffmpeg` is already on the path.

+ ffmpeg_encode_command

> The parameters used by ffmpeg to encode the file.
>
> A list with the most common parameters is available on the botton of this readme.

+ encoded_file_suffix

> Every file encoded by the application will end with this suffix.

+ encoded_file_codec_name

> The codec name that the script will search on the video details to consider it as already encoded.

+ ignored_file_suffix

> A file suffix to signal that a file must be ignored. Usefull to avoid compressing files that should not be compressed.
>
> Ex: Consider a file named file1.avi, if there is an empty file named file1.avi.nocompress, file1.avi will be ignored by the script

+ remove_text_from_file_name

> All text from this list will be removed from the file name

+ replace_text_from_file_name and replace_text_from_file_name_to

> All texts from replace_text_from_file_name list will be replaced by the text from replace_text_from_file_name_to


## File filtering

The script will consider the files with extension ending in one of the extensions defined on the property **video_extensions=**.  
If there is a file on the same directory of the video, with the name of a video file plus the value of the property **ignored_file_suffix=** (set to be ignored), or with the name of the file, ending with the value of the property **encoded_file_suffix=** (already converted), then that video file will be ignored by the script.  

```sh
./video_auto_encode.py -n -f /home/oscar/tmp/

Reading files from /home/oscar/tmp/  - 5 videos files.

Checking existing video files...
 [PASS] - /home/oscar/tmp/3.mp4        - Already encoded to hevc
 [SKIP] - /home/oscar/tmp/1.mp4        - Ignored, converted file exists: 1_H265.mp4
 [PASS] - /home/oscar/tmp/1_H265.mp4   - Already encoded to hevc
 [FAIL] - /home/oscar/tmp/3_VP9.webm   - Need to be converted -> Video: vp9 1920x1080 10 fps Audio: opus, 48000 Hz 
 [SKIP] - /home/oscar/tmp/2.mp4        - Ignored, skip file exists: 2.mp4.skip

------------------------------------------------------------------------------------
1 videos need to be converted - 00:00:00 - 1.13 MB

```

## Noop option

The script has a no-op flag (-n, -nop), this allow to only check the status of the videos, without compressing then.  

## Delete option

There is a flag (-d, --delete), to automatically delete the converted file.  
**Note:** if the converted file is bigger then the source file, the script will delete the converted file and create a empty file with a `.nocompress` suffix, to prevent reencoding this file again.

## Non printable chars

Sometimes on the video metadata information, there are some characters that are unsupported by the python codec (mostly on old 3gp or avi files).  
Since python can not parse the metadata text, these files are going to be ignored by the script.  
In order to convert these files, their metadata information must be stripped manually, using `ffmpeg`.  
The following command can be used to remove these metadata:

```sh
ffmpeg -i my-video.mp4  -map_metadata -1 -c:v copy -c:a copy output.mp4
```

This [shell script](extras/remove_metadata.sh) can also be used to simplify this task

## Common codecs

+ H265 HEVC - Default codec used by the script, this has the better compression

Note: ffmpeg must be compiled with `--enable-libx265` flag

```properties
ffmpeg_encode_command=-i ${INPUT} -c:v libx265 -c:a aac -strict 2 -y -hide_banner -f mp4 ${OUTPUT}
encoded_file_suffix=_H265.mp4
encoded_file_codec_name=hevc
```

+ H264 - The most compatible codec

Note: ffmpeg must be compiled with `--enable-libx264` flag

```properties
ffmpeg_encode_command=-i ${INPUT} -c:v libx264 -c:a aac -strict 2 -y -hide_banner -f mp4 ${OUTPUT}
encoded_file_suffix=_H264.mp4
encoded_file_codec_name=h264
```

+ Using H264 and H265 accelerated using Nvidia hardware (fastest when compatible)

Just add the `-hwaccel cuvid` params before the `${INPUT}` flag and change video codec to `h264_nvenc` or `h265_nvenc`

```properties
ffmpeg_encode_command=-hwaccel cuvid -i ${INPUT} -c:v h264_nvenc -c:a aac -strict 2 -y -hide_banner -f mp4 ${OUTPUT}
```

Notes:

1. ffmpeg must be compiled with `--enable-nvenc` flag
2. The GPU must be compatible with hardware encoding
3. `libvdpau` must be installed

Check out the [ffmpeg hardware accelerated wiki](https://trac.ffmpeg.org/wiki/HWAccelIntro)

+ VP8 - Open source webm format, equivalent to old AVI format, but not so compatible

Note: ffmpeg must be compiled with `--enable-libvpx` flag

```properties
ffmpeg_encode_command=-i ${INPUT} -c:v libvpx -c:a libvorbis -b:v 1M -y -hide_banner -f mkv ${OUTPUT}
encoded_file_suffix=_VP8.webm
encoded_file_codec_name=vp8
```

+ VP9 - Open source VP9 webm format, equivalent to H264, but not so compatible

Note: ffmpeg must be compiled with `--enable-libvpx` flag

```properties
ffmpeg_encode_command=-i ${INPUT} -c:v libvpx-vp9 -c:a libopus -b:v 1M -y -hide_banner -f mkv ${OUTPUT}
encoded_file_suffix=_VP9.webm
encoded_file_codec_name=vp9
```

+ AOMedia AV1 - Open source, experimental and currently too slow, equivalent to H265

Note: ffmpeg must be compiled with `--enable-libaom` flag

```properties
ffmpeg_encode_command=-i ${INPUT} -c:v libaom-av1 -crf 30 -b:v 0 -c:a libopus -strict experimental -y -hide_banner -f mkv ${OUTPUT}
encoded_file_suffix=_AV1.mkv
encoded_file_codec_name=av1
```

For more information about supported codecs, check out the [ffmpeg supported codecs page](https://ffmpeg.org/ffmpeg-codecs.html) or visit the [ffmpeg codec wiki pages](https://trac.ffmpeg.org/).
