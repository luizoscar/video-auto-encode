#!/usr/bin/env bash

othername="${1%.*}_.${1##*.}"
ffmpeg -i $1  -map_metadata -1 -c:v copy -c:a copy $othername
