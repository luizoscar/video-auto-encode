#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
###############################################################################################'
 video_auto_encode.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Class used to encode videos to a specific codec
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from distutils import spawn
import getopt
import os
import sys
import glob
import re
import subprocess
import math
import datetime
import time

def show_usage():
    """
    Show the usage message
    """
    print('Script used to convert videos to a given codec')
    print('')
    print('Usage: video_auto_encode -f <dirname>')
    print('')
    print("    Options:")
    print("    -f, --from [dirname]     Search and encode videos from the directory <dirname>.")
    print('')
    print("    -v, --verbose            Enable verbose mode.")
    print("    -n, --nop                Do not compress videos, only check what would be done.")
    print("    -d, --delete             Delete the original videos after conversion.")
    print("    -h, --help               Show help message.")
    print('')
    sys.exit(0)

def show_error(message):
    """
    Show an error message and terminate the application
    
    @param message: The message to be displayed  
    """
    print('Error: ' + message)
    sys.exit(2)

def show_summary(summary, bytes_encoded, bytes_original):
    """
    Show the conversion summary
    @param bytes_encoded: The total encoded file size in bytes
    @param bytes_original: The total original file size in bytes
    @param summary: The summary lines
    """
    print("------------------------------------------------------------------------------------")
    print("")
    print("Video conversion summary:")

    widths = []
    for row in summary:
        for idx, cell in enumerate(row):
            if len(widths) <= idx:
                widths.append(0)
            widths[idx] = max(len(cell), widths[idx])
                
    for row in summary:
        msg = ""
        for idx, cell in enumerate(row):
            msg = msg + " " + ansi_ljust(cell, widths[idx])
        print(msg)
    
    diff = bytes_original - bytes_encoded
    perc = abs((diff / bytes_original) * 100) 
         
    print("------------------------------------------------------------------------------------")
    if bytes_encoded>bytes_original:
        print("Original files: {} - Encoded files: {} - Expansion ratio: +{:.2f}%".format(human_size(bytes_original), human_size(bytes_encoded), perc))
    else:
        print("Original files: {} - Encoded files: {} - Compression ratio: -{:.2f}%".format(human_size(bytes_original), human_size(bytes_encoded), perc))
    print("------------------------------------------------------------------------------------")
    
    bytes_encoded = 0
    bytes_original = 0
    
def load_settings(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    
    @param comment_char: Possible comment delimiter char
    @param sep: The properties delimiter char
    @param filepath: The properties file path
    """
    if not os.path.isfile(filepath):
        show_error("Config file not found: " + filepath)

    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"') 
                props[key] = value 
    return props

def get_settings(settings, name):
    """
    Get the application settings
    
    @param settings: The application settings
    @param name: The settings name
    """
    if not name in settings:
        show_error("Settings not found: " + name)
    return settings[name]

def human_size(nbytes):
    """
    Convert from bytes to SI format
    
    @param nbytes: The number of bytes 
    """
    bytes_unit = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    human = nbytes
    rank = 0
    if nbytes != 0:
        rank = int((math.log10(abs(nbytes))) / 3)
        rank = min(rank, len(bytes_unit) - 1)
        human = abs(nbytes) / (1024.0 ** rank)
    f = ('%.2f' % human).rstrip('0').rstrip('.')
    return '%s %s' % (f, bytes_unit[rank])
     
def is_video(video_extensions, file_name):
    """
    Check if the file is a video, based on the compatible extensions
    
    @param file_name: The file name
    @param video_extensions: The video file extensions
    """        
    for ext in video_extensions:
        if file_name.lower().endswith(ext.lower()):
            return True
    return False

def get_file_info(path_to_ffmpeg, file_name, verbose):
    """
    Extract the video file information
    
    @param file_name: The file that will be checked
    @param path_to_ffmpeg: The path to ffmpeg
    @param verbose: True to show more messages
    """
    pattern = re.compile("(Duration: [0-9]{2,}:[0-9]{2,}:[0-9]{2,})|(Video: [^\s]+)|([0-9]{2,}x[0-9]{2,})|([0-9|.]+ fps)|(Audio: [^\s]+)|([0-9]+ Hz)")
    
    args = [path_to_ffmpeg, "-hide_banner", "-i", file_name]
    
    ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, encoding="utf-8", universal_newlines=True)
    
    lines = ""
    if verbose:
        print("")
        print("------------------------------------------------------------------------------------")
        print("Executing: " + " ".join(args))
        print("------------------------------------------------------------------------------------")
        
    # Run the process and concatenate all lines
    for line in iter(ffmpeg_process.stdout.readline, ''):
        if (verbose):
            print(line.strip())

        # Ignore garbage lines
        if line.find("Stream #0") or line.find(" Duration:"):
            lines = lines + line
    
    # Get the Regex groups
    resp = ""
    for m in pattern.finditer(lines):
        resp = resp + m.group() + " "
    
    # Terminate the ffmpeg process
    ffmpeg_process.stdout.close()
    if ffmpeg_process.wait() > 1:
        print("Failed to execute ffmpeg, please check the application log.")
    
    if verbose:
        print("")
    return resp  

def get_ffmpeg_params(path_to_ffmpeg, ffmpeg_params, file_name, output_file):
    """
    Build the ffmpeg parameters
    
    @param ffmpeg_params: The codec's ffmpeg parameters
    @param file_name: The input file name
    @param output_file: The output file name
    @param path_to_ffmpeg: The path to ffmpeg  
    """
    resp = [path_to_ffmpeg]
    
    for parm in ffmpeg_params.split(" "):
        if parm == '${INPUT}':
            resp.append(file_name)
        elif parm == '${OUTPUT}':
            resp.append(output_file)
        else:
            resp.append(parm)
            
    return resp

def ansi_ljust(s, width):
    """
    Align a message to the left
    @param s: The message
    @param width: The column widht
    """
    needed = width - len(s)
    if needed > 0:
        return s + ' ' * needed
    else:
        return s

def get_output_file(file_name, encoded_file_suffix, remove_text_from_file_name, replace_text_from_file_name,replace_text_from_file_name_to):
    """
    Get the name of the output file
    
    @param encoded_file_suffix: The output file suffix
    @param file_name: The input file name
    """
    dir_name = os.path.dirname(file_name)
    base_name = os.path.basename(file_name)
    preffix = base_name[:base_name.rfind(".")]
    for suffix in remove_text_from_file_name:
        preffix = preffix.replace(suffix,"") 

    for suffix in replace_text_from_file_name:
        preffix = preffix.replace(suffix,replace_text_from_file_name_to) 
    
    preffix = preffix+ encoded_file_suffix
    
    return dir_name + os.path.sep + preffix.replace(replace_text_from_file_name_to*2,replace_text_from_file_name_to)

def seconds_to_time(secs):
    """
    Convert a time in seconds to HH:MM:SS
    
    @param secs: The number of seconds 
    """

    return time.strftime('%H:%M:%S', time.gmtime(secs))

def time_to_seconds(time):
    """
    Convert a time in the format HH:MM:SS to seconds
    
    @param time: The timestamp
    """
    
    ftr = [3600, 60, 1]
    try:
        return sum([a * b for a, b in zip(ftr, map(int, time.split(':')))])
    except ValueError:
        return 0
    
def get_duration(line):
    """
    Extract the total operation time stamp
    
    @param line: The line to be parsed
    """
    try:
        if "Duration:" in line:
            tmp = line[line.find("Duration:") + 10:]
            tmp = tmp[0: tmp.find(" ")]
        elif "time=" in line:
            tmp = line[line.find("time=") + 5:]
        else:
            return 0
        tmp = tmp[0: tmp.find(".")]
        x = time.strptime(tmp, '%H:%M:%S')
        return datetime.timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()

    except ValueError:
        print("Failed to convert the timestamp from the line: " + line)
    return 0
                    
def check_video_files(videos, max_name_width, ignored_file_suffix, encoded_file_suffix, path_to_ffmpeg, encoded_video_decoder_name, verbose, nop, remove_file_suffix, replace_text_from_file_name, replace_text_from_file_name_to):
    """
    Filter the list of videos files to be encoded
    
    @param encoded_video_decoder_name: Name of the codec
    @param encoded_file_suffix: File suffix to append to the encoded file
    @param ignored_file_suffix: File suffix to the ignored files
    @param max_name_width: The max file name lenght
    @param nop: True for only check the videos
    @param path_to_ffmpeg: The path to ffmpeg
    @param verbose: True to show verbose messages
    @param videos: The list of video files
    """
    # Skip converted and ignored videos
    print("")
    print("Checking video codecs...")

    pre_skip = " [\033[93mSKIP\033[0m]"
    pre_ok = " [\033[92mPASS\033[0m]"
    pre_no = " [\033[91mFAIL\033[0m]"
    
    files_to_compress = []
    total_seconds = 0
    total_bytes = 0
    for file_name in videos:
        skip_file = file_name + ignored_file_suffix
        encoded_file = get_output_file(file_name, encoded_file_suffix,remove_file_suffix, replace_text_from_file_name, replace_text_from_file_name_to)
        if os.path.isfile(skip_file):
            print(pre_skip + ansi_ljust(" - " + file_name, max_name_width) + " - Ignored, skip file exists: " + os.path.basename(skip_file))
        elif os.path.isfile(encoded_file) and encoded_file != file_name:
            print(pre_skip + ansi_ljust(" - " + file_name, max_name_width) + " - Ignored, converted file exists: " + os.path.basename(encoded_file))
        else:
            try:
                details = get_file_info(path_to_ffmpeg, file_name, verbose)
                if encoded_video_decoder_name in details:
                    print(pre_ok + ansi_ljust(" - " + file_name, max_name_width) + " - Already encoded to " + encoded_video_decoder_name)
                else:
                    files_to_compress.append(file_name)
                    total_seconds = total_seconds + get_duration(details)
                    total_bytes = total_bytes + os.path.getsize(file_name)
                    print(pre_no + ansi_ljust(" - " + file_name, max_name_width) + " - Need to be converted -> " + details)
            except UnicodeDecodeError as err:
                print(pre_no + ansi_ljust(" - " + file_name, max_name_width) + " - Failed to get information from file: " + str(err))
    
    print("")
    print("------------------------------------------------------------------------------------")
    print("{} videos need to be converted - {} - {}".format(str(len(files_to_compress)), seconds_to_time(total_seconds), human_size(total_bytes)))
    if len(files_to_compress) == 0:
        print("No videos are going to be converted!")
        sys.exit(0)
    elif nop:
        print("")
        print("Output file name:")
        for file_name in files_to_compress:
            encoded_file = get_output_file(file_name, encoded_file_suffix,remove_file_suffix, replace_text_from_file_name, replace_text_from_file_name_to)
            print("{} -> {}".format(ansi_ljust(" - " + file_name, max_name_width), os.path.basename(encoded_file)))
            
        sys.exit(0)
                
    return files_to_compress      

def delete_converted_videos(converted_videos, encoded_file_suffix, ignored_file_suffix, delete, remove_file_suffix, replace_text_from_file_name, replace_text_from_file_name_to):
    """
    Delete the converted videos, or create a ingore file, if the video was not successfully converte
    @param encoded_file_suffix: File suffix to append to the encoded file
    @param ignored_file_suffix: File suffix to the ignored files
    @param converted_videos: The list of converted files
    @param delete: True to delete after converting
    """
    if delete:
        for file_name in converted_videos:
            output_file = get_output_file(file_name, encoded_file_suffix,remove_file_suffix, replace_text_from_file_name, replace_text_from_file_name_to)
            # If the file was not converted, create a ignore file
            if not os.path.isfile(output_file):
                path = file_name + ignored_file_suffix
                print("Creating ignore file: {}".format(*path))
                with open(path, 'a'):
                    os.utime(path, None)
            else:
                print("Removing file: {}".format(file_name))
                os.remove(file_name)


def parse_and_print_progress(ffmpeg_process, file_name, temp_file):
    """
    Parse the ffmpeg output lines and display a progress bar
    
    @param ffmpeg_process: The ffmpeg process
    """
    # Run the process and concatenate all lines
    # Note: ffmpeg shows the conversion progress on the same line
    total_secs = 0
    total_bytes=0
    current_bytes=0
    if os.path.isfile(file_name):
        total_bytes = os.path.getsize(file_name)
    
    was_progress = False
    for line in iter(ffmpeg_process.stdout.readline, ''):
        if "Duration:" in line:
            total_secs = get_duration(line)
        elif "frame=" in line:
            # Add line break before the first progress bar writing
            if not was_progress:
                print("")

            if os.path.isfile(temp_file):
                current_bytes = os.path.getsize(temp_file)
                
            current_secs = get_duration(line[line.find("frame=") + 7:])

            progress = (float(current_secs) / float(max(total_secs,1))) * 100.0
            progress_int = int(progress)
            bar = '=' * progress_int + '>' + ' ' * (100 - progress_int)
            print("Converting file: [{}] {:.2f}% - {} / {} - ({} / {}){}".format(bar, progress, seconds_to_time(current_secs), seconds_to_time(total_secs), human_size(current_bytes), human_size(total_bytes),' ' *10), end='\r')
            was_progress = True
        elif "Past duration" not in line:
            # Add line break after the last progress bar writing
            if was_progress:
                was_progress = False
                print("\n")
                
            print(line.strip())
            

def delete_if_exist(file_name, title):
    """
    Delete a file is it exists
    @param file_name: The file to be deleted
    @param title: The file title
    """
    if os.path.isfile(file_name):
        print("Removing {} file {}".format(title, file_name))
        os.remove(file_name)
                    
def convert_videos(videos_dir, verbose, nop, delete):   
    """
    Check and convert the video files
    @param verbose: True to show verbose messages
    @param nop: True for only check the videos
    @param delete: True to delete after converting
    @param videos_dir: The videos directory
    """
    
    # Read the appplication settings
    config_file = os.path.dirname(os.path.realpath(__file__)) + os.sep + "video_auto_encode.properties"
    settings = load_settings(config_file)
    video_extensions = get_settings(settings, 'video_extensions').split('|')
    path_to_ffmpeg = get_settings(settings, 'path_to_ffmpeg')
    ffmpeg_params = get_settings(settings, 'ffmpeg_encode_command');
    encoded_file_suffix = get_settings(settings, 'encoded_file_suffix')
    ignored_file_suffix = get_settings(settings, 'ignored_file_suffix')
    encoded_video_decoder_name = get_settings(settings, 'encoded_file_codec_name')

    remove_text_from_file_name = get_settings(settings, 'remove_text_from_file_name').split('|')
    replace_text_from_file_name = get_settings(settings, 'replace_text_from_file_name').split('|')
    replace_text_from_file_name_to= get_settings(settings, 'replace_text_from_file_name_to')
    
    # Check if ffmpeg is accessible
    if not spawn.find_executable(path_to_ffmpeg):
        show_error("Unable to locate ffmpeg from " + path_to_ffmpeg)
    
    # Build the file list
    print("Reading files from {}".format(videos_dir))
    source_file_list = glob.glob(videos_dir + '/**/*.*', recursive=True)
    source_file_list.sort()
    
    max_name_width = 0
    videos = []
    for file_name in source_file_list:
        if is_video(video_extensions, file_name) and os.stat(file_name).st_size > 0:
            max_name_width = max(max_name_width, len(file_name))
            videos.append(file_name)
    
    max_name_width = max_name_width + 5
    print("Found {} video file(s). ".format(str(len(videos))))

    # Check the videos that will be encoded
    files_to_compress = check_video_files(videos, max_name_width, ignored_file_suffix, encoded_file_suffix, path_to_ffmpeg, encoded_video_decoder_name, verbose, nop, remove_text_from_file_name, replace_text_from_file_name, replace_text_from_file_name_to)
        
    # Convert the videos
    converted_videos = []
    bytes_encoded = 0
    bytes_original = 0
    summary = []

    for idx, file_name in enumerate(files_to_compress):
        output_file = get_output_file(file_name, encoded_file_suffix,remove_text_from_file_name, replace_text_from_file_name, replace_text_from_file_name_to)
        temp_file = get_output_file(file_name, ".tmp",remove_text_from_file_name, replace_text_from_file_name,replace_text_from_file_name_to)

        delete_if_exist(temp_file, "temp")
        delete_if_exist(output_file, "output")
        
        # Note: Set the ffmpeg output for a temp file, so the input file will be processed again if the script was interrupted       
        args = get_ffmpeg_params(path_to_ffmpeg, ffmpeg_params, file_name, temp_file)
        print("------------------------------------------------------------------------------------")
        print("Converting file {} / {}: {} -> {}".format(idx + 1, len(files_to_compress), file_name, output_file))
        print("------------------------------------------------------------------------------------")
        print("Executing: " + " ".join(args))
        print(" ")
        ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)
        
        parse_and_print_progress(ffmpeg_process, file_name, temp_file)

        print("")
        # Terminate the converted file size
        ffmpeg_process.stdout.close()
        if ffmpeg_process.wait() > 1:
            print("Failed to execute ffmpeg, this video will be skipped.")
            delete_if_exist(temp_file, "temp")
            delete_if_exist(output_file, "output")
        else:
            # After completion, rename the temp file
            delete_if_exist(output_file, "output")
            print(" - Renaming {} -> {}".format(os.path.basename(temp_file), os.path.basename(output_file)))
            os.rename(temp_file, output_file)

        converted_videos.append(file_name)
        
        # Show converted video summary
        input_size = os.path.getsize(file_name)
        output_size = os.path.getsize(output_file)
        bytes_encoded = bytes_encoded + output_size
        bytes_original = bytes_original + input_size

        diff = input_size - output_size
        perc = abs((diff / input_size) * 100)    

        outcome = "(\033[92m-{} / -{:.2f}%\033[0m)" if output_size < input_size else "(\033[91m+{} / +{:.2f}%\033[0m)"
        outcome = outcome.format(human_size(diff), perc)

        msg = "{} | -> {} | {} vs {} | {}".format(file_name, os.path.basename(output_file), human_size(input_size), human_size(output_size), outcome)
        summary.append(msg.split("|"))
        print(" - Converted: "+msg)
        print("")
    
    show_summary(summary, bytes_encoded, bytes_original)
    delete_converted_videos(converted_videos, encoded_file_suffix, ignored_file_suffix, delete, remove_text_from_file_name, replace_text_from_file_name, replace_text_from_file_name_to)
         
    sys.exit(0)
        
def main():

    # Parse the application parameters
    verbose = False
    nop = False
    delete = False
    videos_dir = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:vnd", ["help", "from=", "verbose", "nop", "delete"])
    except getopt.GetoptError:
        show_usage()
    for o, a in opts:
        if o in ("-v", "--verbose"):
            verbose = True
        elif o in ("-n", "--nop"):
            nop = True
        elif o in ("-d", "--delete"):
            delete = True
        elif o in ("-h", "--help"):
            show_usage()
            sys.exit()
        elif o in ("-f", "--from"):
            videos_dir = a
        else:
            assert False, "unhandled option"
    
    if videos_dir is not None:
        convert_videos(videos_dir, verbose, nop, delete)
    else:
        show_error("You need to specify the video dir using the -f parameter")

if __name__ == "__main__":
    main()
